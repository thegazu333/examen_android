package com.example.naruto.ejercicio;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class PrincipalActivity extends AppCompatActivity {

    private EditText password, confpassword, nombre, apellido;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
        password= (EditText) findViewById(R.id.et3);
        confpassword= (EditText) findViewById(R.id.et4);

    }

    public void login (View view)
    {
        String pas=password.getText().toString();
        String conpas=confpassword.getText().toString();

        if (pas.equals(conpas))
        {
            Intent i=new Intent(this,ventana2Activity.class);
            startActivity(i);
        }
        else {
            Toast notificarion = Toast.makeText(this, "Las contrasenas no conciden", Toast.LENGTH_SHORT);
            notificarion.show();

        }
        Toast notificarion = Toast.makeText(this, "Logueado", Toast.LENGTH_SHORT);
        notificarion.show();
    }
}
